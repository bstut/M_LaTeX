extern crate markdown;

use markdown::Block;

use std::error::Error;
use std::io::prelude::*;
use std::fs::File;
use std::path::Path;

pub fn fuse_latex(ltx_vec: Vec<String>) -> File {
    let mut base = String::new();
    
    let path = Path::new("out.tex");
    let display = path.display();
    
    for line in ltx_vec {
        base.push_str(&line);
    }

    let mut output = match File::create(&path) {
        Err(why) => panic!("couldn't create {}: {}", display, why.description()),
        Ok(output) => output,
    };

    match output.write_all(base.as_bytes()) {
        Err(why) => {
            panic!("couldn't write to {}: {}", display,
                                               why.description())
        },
        Ok(_) => println!("successfully wrote to {}", display),
    }

    output
}

pub fn parse_md<P> (filename: P) -> Vec<Block>
    where
    P: AsRef<Path>
{
    let mut file = File::open(filename).expect("Error.");
    let mut file_str = String::new();
    file.read_to_string(&mut file_str).expect("Could not parse file to string");

    markdown::tokenize(&file_str)
}
