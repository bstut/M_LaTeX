extern crate markdown;

mod file_handler;
mod latex;

use latex::doc_struct::LatexDoc;
use latex::latex::*;
use std::path::Path;
use std::env;

fn main() {
    //parse command line arguments into vector
    let cmd_args: Vec<_> = env::args().collect();

    // test with default values
    // replace with commandline-arg-based approach
    let doc_class = (["11pt"], "article");
    let authors = ["Default Valueman"];
    let title = "Default title";
    let language = "ngerman";
    let packages = ["amsmath"];

    let head_vec = vec![
        create_doc_class(&doc_class.0, doc_class.1),
        head_vec_from_vec(&authors, 0),
        head_from_string(language, 1),
        head_from_string(title, 0),
        head_vec_from_vec(&packages, 1),
    ];

    let path = Path::new(&cmd_args[1]);
    let file = file_handler::parse_md(path);
    //let preltx_vec = parser::to_latex(&file);

    let body_vec = block_vec_to_body_vec(&file);

    let document = LatexDoc { head : head_vec, body : body_vec };
    
    let doc_vec = document.doc_to_string_vec();

    file_handler::fuse_latex(doc_vec);
}
