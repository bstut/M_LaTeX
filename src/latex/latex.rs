use markdown::Block as mBlock;
use markdown::Span as mSpan;
use markdown::ListItem;

pub enum Head {
    // \documentclass[str0,str1,...]{str}
    DocClass(Vec<String>,String),

    // incategoryation on author, title, etc.
    Authors(Vec<String>),
    Title(String),
    Language(String),

    Packages(Vec<String>)
}

pub enum Body {
    Section(Vec<Span>, usize),
    Paragraph(Vec<Span>),
    List(Vec<ListItems>),
    Blockquote(Vec<Body>),
    CodeBlock(String),
    Raw(String)
}

pub enum Span {
    Text(String),
    Code(String),

    Link(String, String),
    Image(String, String, Option<String>),

    Emphasis(Vec<Span>),
    Strong(Vec<Span>)
}

pub enum ListItems {
    Simple(Vec<Span>),
    Paragraph(Vec<Body>)
}

// 0: Title, 1: language
pub fn head_from_string(content: &str, category: usize) -> Head {
    let ret;
    
    if category == 0 {
        ret = Head::Title(content.to_owned())
    } else {
        ret = Head::Language(content.to_owned())
    }

    ret
}

// 0: Authors, 1: Packages
pub fn head_vec_from_vec(content: &[&str], category: usize) -> Head {
    let ret;

    if category == 0 {
        ret = Head::Authors(content.iter().map(|&s|s.to_owned()).collect())
    } else {
        ret = Head::Packages(content.iter().map(|&s|s.to_owned()).collect())
    }

    ret
}

pub fn create_doc_class(options: &[&str], class: &str) -> Head {
    Head::DocClass(
        options.iter().map(|&s|s.to_owned()).collect(),
        class.to_owned()
    )
}

pub fn block_vec_to_body_vec(block: &[mBlock]) -> Vec<Body> {
    let mut ret = Vec::new();
    let mut tmp;
    
    for item in block {
        tmp = block_to_body(&item);
        ret.push(tmp);
    }
    
    ret
}

pub fn block_to_body(block: &mBlock) -> Body {
    match *block {
        mBlock::Header(ref vector, index) =>
            Body::Section(span_vec_to_ltxspan_vec(vector), index),

        mBlock::Paragraph(ref vector) =>
            Body::Paragraph(span_vec_to_ltxspan_vec(vector)),

        mBlock::Blockquote(ref vector) =>
            Body::Blockquote(block_vec_to_body_vec(vector)),

        mBlock::CodeBlock(ref string) =>
            //TODO: import lstlisting only if necessary
            Body::CodeBlock(string.to_owned()),

        mBlock::UnorderedList(ref vector) =>
            Body::List(format_list_vec(vector)),
        
        mBlock::Raw(ref string) =>
            Body::Raw(string.to_owned()),

        mBlock::Hr => Body::Raw("\\hrule\n".to_string())
    }
}

pub fn span_vec_to_ltxspan_vec(block: &[mSpan]) -> Vec<Span> {
    let mut ret = Vec::new();
    let mut tmp;
    
    for item in block {
        tmp = span_to_ltxspan(&item);
        ret.push(tmp);
    }
    
    ret
}

pub fn span_to_ltxspan(content: &mSpan) -> Span {
    match *content {
        mSpan::Break =>
            Span::Text("\\\\".to_owned()),

        mSpan::Text(ref text) =>
            Span::Text(text.to_owned()),
        
        mSpan::Code(ref text) =>
            Span::Code(text.to_owned()),

        mSpan::Link(ref text, ref url, _) =>
            Span::Link(text.to_owned(), url.to_owned()),

        mSpan::Image(ref text, ref url, None) =>
            Span::Image(text.to_owned(), url.to_owned(), None),

        mSpan::Image(ref text, ref url, Some(ref title)) =>
            Span::Image(text.to_owned(), url.to_owned(), Some(title.to_owned())),

        mSpan::Emphasis(ref vec) =>
            Span::Emphasis(span_vec_to_ltxspan_vec(vec)),

        mSpan::Strong(ref vec) =>
            Span::Strong(span_vec_to_ltxspan_vec(vec))
    }
}

pub fn format_list_vec(content: &[ListItem]) -> Vec<ListItems> {
    let mut ret = Vec::new();

    for item in content {
        ret.push(format_list(&item))
    }

    ret
}

pub fn format_list(item: &ListItem) -> ListItems {
    match *item {
        ListItem::Simple(ref vec) =>
            ListItems::Simple(span_vec_to_ltxspan_vec(vec)),

        ListItem::Paragraph(ref vec) =>
            ListItems::Paragraph(block_vec_to_body_vec(vec))
    }
}

pub fn head_to_string(item: &Head) -> String {
    let mut ret = String::new();
    
    match *item {
        Head::DocClass(ref options, ref class) => {
            ret.push_str("\\documentclass[");

            for option in options {
                ret.push_str(&format!("{},", option))
            }

            ret.push_str(&format!("]{{{}}}\n", class))
        },

        Head::Authors(ref authors) => {
            ret.push_str("\\author{");
            
            for author in authors {
                ret.push_str(&format!("{},", author))
            }

            ret.push_str("}\n")
        },

        Head::Title(ref title) => ret.push_str(&format!("\\title{{{}}}\n", title)),

        Head::Language(ref language) => { //TODO
            match language.as_ref() {
                "de_DE" => ret.push_str("\\usepackage[ngerman]{babel}\n"),
                _       => ret.push_str("\\usepackage[english]{babel}\n"),
            }
        },

        Head::Packages(ref packages) => {
            ret.push_str("\\usepackage{");

            for package in packages {
                ret.push_str(&format!("{},", package))
            }

            ret.push_str("}\n")
        }
    }

    ret
}

pub fn head_vec_to_string(head: &[Head]) -> String {
    let mut ret = String::new();

    for elem in head {
        ret.push_str(&head_to_string(&elem))
    }

    ret
}

pub fn body_to_string(item: &Body) -> String {
    match *item {
   	Body::Section(ref title, depth) =>
            section_to_string(title, depth),
   	
   	Body::Paragraph(ref span_vec) =>
            span_vec_to_string(span_vec),

        Body::List(ref list) =>
            list_vec_to_string(list),
            
   	Body::Blockquote(ref body_vec) =>
            format!("\n\\indent {}", body_vec_to_string(body_vec)),
   	
   	Body::CodeBlock(ref string) =>
   	//TODO: import lstlisting only if necessary
   	    format!("\\begin{{lstlisting}}\n{}\n\\end{{lstlisting}}\n", string),
   	
	Body::Raw(ref string) => string.to_owned(),
    }
}

pub fn body_vec_to_string(body: &[Body]) -> String {
    let mut ret = String::new();

    for elem in body {
        ret.push_str(&body_to_string(&elem))
    }
    
    ret.push_str("\n");

    ret
}

fn section_to_string(title: &[Span], depth: usize) -> String {
    let mut ret = String::new();
    let span_string = span_vec_to_string(title);
    
    ret.push_str("\\");

    if depth < 3 {
        if depth == 1 {
            ret.push_str("sub")
        } else {
            ret.push_str("subsub")
        }
        ret.push_str(&format!("section*{{{}}}\n", span_string))
    } else {
        ret.push_str(&format!("paragraph{{{}}}\n", span_string))
    }

    ret
}

pub fn span_to_string(span: &Span) -> String {
    match *span {
        Span::Text(ref text) =>
            format!("{}\n", text),

        Span::Code(ref text) =>
            format!("\\begin{{lstlisting}}\n{}\n\\end{{lstlisting}}\n", text),

        Span::Link(ref text, ref url) =>
            format!("\\href{{{}}}{{{}}}\n", url, text),
            
        Span::Image(ref text, ref url, None) =>
            format!("\\begin{{figure}}\n\\includegraphics[width=\\linewidth]{{{}}}\n\\caption{{{}}}\n\\end{{figure}}\n",
                    url,
                    text),

        Span::Image(ref text, ref url, Some(ref title)) =>
            format!("\\begin{{figure}}\n\\includegraphics[width=\\linewidth]{{{}}}\n\\caption{{{}}}\n\\label{{{}}}\n\\end{{figure}}\n",
                        url,
                        title,
                        text),

        Span::Emphasis(ref content) =>
            format!("\\emph{{{}}}\n", span_vec_to_string(content)),        

        Span::Strong(ref content) =>
            format!("\\textbf{{{}}}\n", span_vec_to_string(content)),
    }
}

pub fn span_vec_to_string(span: &[Span]) -> String {
    let mut ret = String::new();

    for elem in span {
        ret.push_str(&span_to_string(&elem))
    }

    ret
}

pub fn list_to_string(item: &ListItems) -> String {
    match *item {
        ListItems::Simple(ref content) =>
            span_vec_to_string(content),

        ListItems::Paragraph(ref content) =>
            body_vec_to_string(content)
    }
}

pub fn list_vec_to_string(list: &[ListItems]) -> String {
    let mut ret = String::new();

    for item in list {
        ret.push_str(&list_to_string(&item))
    }

    ret
}
