use latex::latex::*;

pub struct LatexDoc {
    pub head: Vec<Head>,
    pub body: Vec<Body>
}

impl LatexDoc {
    pub fn head_to_string(&self) -> String {
        head_vec_to_string(&self.head)
    }

    pub fn body_to_string(&self) -> String {
        format!("{}\n{}\n{}",
                "\\begin{document}",
                body_vec_to_string(&self.body),
                "\\end{document}"
        )
    }
    
    pub fn doc_to_string_vec(&self) -> Vec<String> {
        vec![self.head_to_string(), self.body_to_string()]
    }
}
